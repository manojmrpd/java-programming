package com.string.reverse;

import java.util.Scanner;

public class ReverseStringAndCheckPalindrome {

	private static Scanner s;

	public static void main(String[] args) {
		
		s = new Scanner(System.in);
		System.out.println("Please enter a string: ");
		String str = s.next();
		System.out.println("Original string: "+str);
		String rev="";
		for(int i=str.length()-1;i>=0;i--) {
			rev = rev+str.charAt(i);
		}
		System.out.println("Reverse string: "+rev);
		if(str.equals(rev)) {
			System.out.println(str+" Is a Palindrome string");
		} else {
			System.out.println(str+" Is Not a Palindrome string");
		}

	}

}
