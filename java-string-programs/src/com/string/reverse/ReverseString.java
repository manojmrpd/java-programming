package com.string.reverse;

import java.util.Scanner;

public class ReverseString {
	
	private static Scanner s;

	public static void main(String[] args) {

		s = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String str = s.next();
		
		String rev="";
		for(int i=str.length()-1;i>=0;i--) {
			rev = rev + str.charAt(i);
		}
		
		System.out.println(rev);
		if(str.equalsIgnoreCase(rev)) {
			System.out.println(str+" is a palindrome");
		} else {
			System.out.println(str+" is not a palindrome");
		}
		
	}

}
