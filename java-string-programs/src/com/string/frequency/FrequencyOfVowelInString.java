package com.string.frequency;

import java.util.Scanner;

public class FrequencyOfVowelInString {

	private static Scanner s;

	public static void main(String[] args) {
		s = new Scanner(System.in);
		System.out.println("Enter a string name: ");
		String str = s.next();
		int count=0;
		for (int i = 0; i <= str.length() - 1; i++) {
			if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o'
					|| str.charAt(i) == 'u') {
				count++;

			}
		}
		System.out.println("No of the vowles in a given string is: "+count);

	}

}
