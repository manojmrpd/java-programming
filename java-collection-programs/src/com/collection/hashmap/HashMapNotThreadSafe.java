package com.collection.hashmap;

//Java program to illustrate 
//HashMap drawbacks 
import java.util.HashMap; 

public class HashMapNotThreadSafe extends Thread  {
	
	private static HashMap<Integer,String> l=new HashMap<Integer,String>(); 

	@Override
	public void run() 
	{ 	
		try { 
			Thread.sleep(1000); 
			l.put(103,"D"); 
		} catch(InterruptedException e)  { 
			e.printStackTrace();
		} 
	} 

	public static void main(String[] args) throws InterruptedException 
	{ 
		l.put(100,"A"); 
		l.put(101,"B"); 
		l.put(102,"C"); 
		Thread.sleep(1000);
		HashMapNotThreadSafe t=new HashMapNotThreadSafe(); 
		t.start(); 
		
		for (Object object : l.entrySet()) 
		{ 
			System.out.println(object); 
			Thread.sleep(1000); 
		} 
		System.out.println(l); 
	} 
} 

