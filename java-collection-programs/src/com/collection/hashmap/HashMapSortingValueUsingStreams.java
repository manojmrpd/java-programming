package com.collection.hashmap;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class HashMapSortingValueUsingStreams {
	
	private static HashMap<String, Employee> hm = new HashMap<>();
	
	public static void main(String[] args) {
		
		hm.put("Kate", new Employee(1, "Kate", 27, 200));
		hm.put("John", new Employee(3, "John", 23, 300));
		hm.put("Gene", new Employee(4, "Gene", 24, 100));
		hm.put("Andrew", new Employee(2, "Andrew", 21, 500));
		hm.put("Winson", new Employee(5, "Winson", 18, 400));
		hm.put("Sofi", new Employee(6, "Sofi", 19, 200));
		
		LinkedHashMap<String, Employee> map = hm.entrySet().stream().sorted(Map.Entry.<String, Employee>comparingByValue())
		.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(key, value) -> value, LinkedHashMap::new));
		
		for(Map.Entry<String, Employee> m : map.entrySet()) {
			Employee employee = m.getValue();
			System.out.println("id:"+employee.getEmpId()+ " name:"+employee.getEmpName());
		}
	}

}
