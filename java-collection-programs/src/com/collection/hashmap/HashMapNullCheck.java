package com.collection.hashmap;

import java.util.HashMap;
/*
 * 1. HashMap will allow duplicates values to insert but it will not allow duplicate keys. 
 *         If you pass duplicate key it will override the value of second key 
 * 
 * 2. HashMap will allow null values to insert but it will allow only one null key
 *         If you pass second null key, it will replace the value of second key
 *         
 * 3. HashMap is not thread-safe and not synchronized
 */

public class HashMapNullCheck {

	private static HashMap<Integer, String> hm = new HashMap<>();

	public static void main(String[] args) {
		
		hm.put(101, "A");
		hm.put(102, "B");
		hm.put(103, "A"); // It will allow duplicates values to insert but
		hm.put(107, "C"); // It will not allow duplicate keys and it will override the duplicate keys with latest
		hm.put(null, "D"); 
		hm.put(null, "E"); //It Will allow 1 null key and for second null key it will replace the value of second null key.
		hm.put(104, null); 
		hm.put(105, null);
		System.out.println(hm);
	}

}
