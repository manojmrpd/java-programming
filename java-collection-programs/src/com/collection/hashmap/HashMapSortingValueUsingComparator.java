package com.collection.hashmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;import java.util.Map;

public class HashMapSortingValueUsingComparator {
	
	private static HashMap<Integer, Employee> hm = new HashMap<>();

	public static void main(String[] args) {
		
		hm.put(1, new Employee(1, "Kate", 27, 200));
		hm.put(3, new Employee(3, "John", 23, 300));
		hm.put(4, new Employee(4, "Gene", 24, 100));
		hm.put(2, new Employee(2, "Andrew", 21, 500));
		hm.put(5, new Employee(5, "Winson", 18, 400));
		hm.put(6, new Employee(6, "Sofi", 19, 200));
		
		ArrayList<Employee> list = new ArrayList<>();
		for(Map.Entry<Integer, Employee> map: hm.entrySet()) {
			Employee employee = map.getValue();
			list.add(employee);
		}
		
		Collections.sort(list, new Comparator<Employee>() {

			@Override
			public int compare(Employee e1, Employee e2) {
				return e1.compareTo(e2);
			}
		});
		for(Employee e: list) {
			System.out.println(e.getEmpId()+" "+e.getEmpName()+ " "+e.getSalary());
		}
				
		
		
		
		
		
		
	}

}
