package com.collection.arraylist;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ConvertArrayListToLinkedListUsingPlainJava {

	public static void main(String[] args) {
		
		List<String> al = Arrays.asList("tom","antony","jennifer");
		System.out.println(al);
		LinkedList<String> ll = new LinkedList<>(al);
		System.out.println(ll);
	}
}
