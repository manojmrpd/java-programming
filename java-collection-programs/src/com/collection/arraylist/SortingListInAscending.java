package com.collection.arraylist;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingListInAscending {

	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("US","IND","AUS","ENG","WI","PAK","NZ","ZIM","SA");
		Collections.sort(list, new Comparator<String>() {

			@Override
			public int compare(String s1, String s2) {
				return s1.compareTo(s2);
			}
		});
		System.out.println(list);
	}

}
