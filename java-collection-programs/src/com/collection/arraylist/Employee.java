package com.collection.arraylist;

public class Employee {
	private Integer eId;
	private String empName;
	private Integer age;
	private Integer salary;
	public Integer geteId() {
		return eId;
	}
	public void seteId(Integer eId) {
		this.eId = eId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getSalary() {
		return salary;
	}
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	public Employee(Integer eId, String empName, Integer age, Integer salary) {
		super();
		this.eId = eId;
		this.empName = empName;
		this.age = age;
		this.salary = salary;
	}
	Employee() {
		super();
	}
}