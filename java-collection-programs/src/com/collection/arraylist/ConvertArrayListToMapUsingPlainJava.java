package com.collection.arraylist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConvertArrayListToMapUsingPlainJava {

	public static void main(String[] args) {
		Map<Integer,Employee> hm = new HashMap<>();
		List<Employee> al = new ArrayList<>();
		al.add(new Employee(1, "jennifer", 25, 200));
		al.add(new Employee(2, "rose", 23, 350));
		al.add(new Employee(3, "kate", 24, 240));
		
		for(Employee e:al) {
			hm.put(e.geteId(), e);
		}
		for(Map.Entry<Integer, Employee> e : hm.entrySet()) {
			System.out.println("Key: "+e.getKey()+ " Value: "+e.getValue().getEmpName());	
		}
	}
}

