package com.collection.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ComparableExample {

	public static void main(String[] args) {
		
		ArrayList<Employee> list = new ArrayList<>();
		list.add(new Employee(1,"Jean",21));
		list.add(new Employee(2,"Antony",25));
		list.add(new Employee(3,"Steve",23));

		Collections.sort(list, new Comparator<Employee>() {
			@Override
			public int compare(Employee e1, Employee e2) {
				return e1.compareTo(e2);
			}
		});
		for(Employee e:list) {
			System.out.println(e.getEmpId()+" "+e.getEmpName()+" "+e.getAge());
		}
		
		ArrayList<User> users = new ArrayList<>();
		users.add(new User(1, "Manoj", 21));
		users.add(new User(3, "Pavan", 25));
		users.add(new User(2, "Kalyan", 23));
		Collections.sort(users, new Comparator<User>() {
			@Override
			public int compare(User u1, User u2) {
				if (u1.getAge()==u2.getAge()) {
					return 0;
				} else if(u1.getAge() > u2.getAge()) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		for(User e:users) {
			System.out.println(e.getUserId()+" "+e.getUserName()+" "+e.getAge());
		}

	}
}
