package com.collection.comparable;

public class Employee implements Comparable<Employee> {
	
	private Integer empId;
	private String empName;
	private Integer age;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Employee(Integer empId, String empName, Integer age) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.age = age;
	}
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public int compareTo(Employee e1) {
		if(age==e1.age) {
		return 0;
		} else if (age>e1.age) {
			return 1;
		} else {
			return -1;
		}
	}
}
