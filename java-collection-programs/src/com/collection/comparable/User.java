package com.collection.comparable;

public class User  {

	private Integer userId;
	private String userName;
	private Integer age;

	User(Integer userId, String userName, Integer age) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.age = age;
	}

	User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
