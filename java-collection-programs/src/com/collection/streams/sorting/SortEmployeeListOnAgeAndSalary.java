package com.collection.streams.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.collection.streams.conversion.Employee;

public class SortEmployeeListOnAgeAndSalary {

	public static void main(String[] args) {
		
		List<Employee> al = new ArrayList<>();
		al.add(new Employee(1, "jennifer", 25, 200));
		al.add(new Employee(1, "jean", 25, 200));
		al.add(new Employee(3, "rose", 23, 350));
		al.add(new Employee(2, "kate", 24, 240));
		
		// Given a list of employees, sort all the employee on the basis of Salary
		List<Employee> salarySort = al.stream().sorted((e1, e2) -> (e1.getSalary() - e2.getSalary())).collect(Collectors.toList());
		for(Employee e:salarySort) {
			System.out.println(e.getEmpId()+ " "+e.getEmpName()+ " " +e.getAge()+ " "+e.getSalary());
		}
		System.out.println("=============================");
		
		// Given a list of employees, sort all the employee on the basis of Age
		List<Employee> ageSort = al.stream().sorted((e1,e2) -> (e1.getAge() - e2.getAge())).collect(Collectors.toList());
		for(Employee e:ageSort) {
			System.out.println(e.getEmpId()+ " "+e.getEmpName()+ " " +e.getAge()+ " "+e.getSalary());
		}
	}
}
