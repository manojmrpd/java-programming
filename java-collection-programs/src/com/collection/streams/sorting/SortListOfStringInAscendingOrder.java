package com.collection.streams.sorting;

import java.util.Arrays;
import java.util.List;

public class SortListOfStringInAscendingOrder {

	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("US","IND","AUS","ENG","WI","PAK","NZ","ZIM");
		list.stream().sorted().forEach((x) -> System.out.println(x));

	}

}
