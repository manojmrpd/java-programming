package com.collection.streams.sorting;

import java.util.Arrays;
import java.util.List;

public class SortListOfStringInDescendingOrder {

	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("US","IND","AUS","ENG","WI","PAK","NZ","ZIM");
		list.stream().sorted((o1, o2) -> o2.compareTo(o1)).forEach((x) -> System.out.println(x));
	}
}
