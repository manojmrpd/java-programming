package com.collection.streams.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.collection.streams.conversion.Employee;

public class FilterEmployeeOnAgeAndSalary {
	
	public static void main(String[] args) {
		
		List<Employee> al = new ArrayList<>();
		al.add(new Employee(1, "jennifer", 30, 200));
		al.add(new Employee(1, "gene", 25, 200));
		al.add(new Employee(3, "rose", 26, 350));
		al.add(new Employee(2, "kate", 24, 240));
		
		//Given a list of employees, you need to filter all the employee whose age is greater than 25 and print the employee names
		System.out.println("Get Employees Whose Age is Greater than 25");
		List<String> ageList = al.stream()
                .filter(e->e.getAge()>25)
                .map(Employee::getEmpName)
                .collect(Collectors.toList());
		ageList.forEach(System.out::println);
		
		//Given a list of employees, you need to filter all the employee whose salary is greater than 200 and print the employee names
		System.out.println("Get Employees Whose Salary is Greater than 200");
		List<String> salaryList = al.stream()
                .filter(e->e.getSalary()>200)
                .map(Employee::getEmpName)
                .collect(Collectors.toList());
		salaryList.forEach(System.out::println);
	}

}
