package com.collection.streams.conversion;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayListToLinkedListUsingStreams {

	public static void main(String[] args) {
		
		List<String> al = Arrays.asList("tom","jennifer","antony","kim");
		System.out.println(al);
		LinkedList<String> ll = al.stream().collect(Collectors.toCollection(LinkedList::new));
		System.out.println(ll);
		
		List<Employee> list = Arrays.asList(new Employee(1, "Jean", 23, 200),
				new Employee(2, "tom", 21, 100));
		LinkedList<Employee> linkedlist = list.stream().collect(Collectors.toCollection(LinkedList::new));
		System.out.println(linkedlist);
	}
}
