package com.collection.streams.conversion;

public class Employee implements Comparable<Employee> {
	
	private Integer empId;
	private String empName;
	private Integer age;
	private Integer salary;
	
	public Integer getSalary() {
		return salary;
	}
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	public Employee(Integer empId, String empName, Integer age, Integer salary) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.age = age;
		this.salary = salary;
	}
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public int compareTo(Employee e1) {
		if(empId==e1.getEmpId()) {
			return 0;
		} else if(empId > e1.getEmpId()) {
			return 1;
		} else {
			return -1;
		}
	}
	
}
