package com.collection.streams.conversion;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ArrayListOfEmployeeToMapUsingStreams {

	public static void main(String[] args) {
		List<Employee> al = new ArrayList<>();
		al.add(new Employee(1, "jennifer", 25, 200));
		al.add(new Employee(1, "jean", 25, 200));
		al.add(new Employee(3, "rose", 23, 350));
		al.add(new Employee(2, "kate", 24, 240));
		al.add(new Employee(2, "jean", 24, 240));

		// Approach-1 : Using Collectors.toMap()
		LinkedHashMap<Integer, String> toMap = al.stream().collect(
				Collectors.toMap(Employee::getEmpId, Employee::getEmpName, 
						(x, y) -> x + ", " + y, LinkedHashMap::new));
		toMap.forEach((x, y) -> System.out.println(x + "=" + y));

		// Approach-2 : Using Collectors.groupingBy()
		Map<Integer, List<String>> groupingBy = al.stream().collect(
				Collectors.groupingBy(Employee::getEmpId, 
				Collectors.mapping(Employee::getEmpName, Collectors.toList())));
		
		groupingBy.forEach((x, y) -> System.out.println(x + "=" + y));
	}
}
