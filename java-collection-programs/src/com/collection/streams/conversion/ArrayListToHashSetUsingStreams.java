package com.collection.streams.conversion;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayListToHashSetUsingStreams {

	public static void main(String[] args) {
		List<String> al = Arrays.asList("tom","jennifer","antony","kim");
		HashSet<String> hs = al.stream().collect(Collectors.toCollection(HashSet::new));
		System.out.println(hs);
		
		List<Employee> list = Arrays.asList(new Employee(1, "Jean", 23, 200),
				new Employee(2, "tom", 21, 100));
		HashSet<Employee> employees = list.stream().collect(Collectors.toCollection(HashSet::new));
		System.out.println(employees);
	}
}
