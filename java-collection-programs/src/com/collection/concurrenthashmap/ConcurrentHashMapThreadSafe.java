package com.collection.concurrenthashmap;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapThreadSafe  extends Thread {
	
	private static ConcurrentHashMap<Integer, String> chm = new ConcurrentHashMap<>();
	
	@Override
	public void run() {
		try {
			Thread.sleep(1000);
			chm.put(104, "D");
			System.out.println(chm);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		chm.put(101, "A");
		chm.put(102, "B");
		chm.put(103, "C");
		Thread.sleep(1000);
		ConcurrentHashMapThreadSafe demo = new ConcurrentHashMapThreadSafe();
		demo.start();
		
		for(Object object: chm.entrySet()) {
			System.out.println(object);
			Thread.sleep(1000);
		}
	}

}
