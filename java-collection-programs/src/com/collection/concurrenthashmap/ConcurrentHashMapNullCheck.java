package com.collection.concurrenthashmap;

import java.util.concurrent.ConcurrentHashMap;
/*
 * 1. ConcurrentHashMap will allow duplicates values to insert but it will not allow duplicate keys. 
 *         If you pass duplicate key it will override the value of second key.
 * 
 * 2. ConcurrentHashMap will not allow null key and null value. It will throw null pointer exception
 *         
 * 3. ConcurrentHashMap is Thread-safe and Synchronized.
 */

public class ConcurrentHashMapNullCheck {
	
	private static ConcurrentHashMap<Integer, String> chm = new ConcurrentHashMap<>();

	public static void main(String[] args) {
		
		chm.put(101, "A");
		chm.put(102, "B");
		chm.put(103, "A"); // It will allow duplicates values to insert but
		chm.put(101, "C"); // It will not allow duplicate keys and it will override the duplicate keys with latest
		//chm.put(null, "D"); //It Will throw Null Pointer exception as it doesn't allow null keys
		//chm.put(104, null); //It Will throw Null pointer exception as it doesn't allow null values
		System.out.println(chm);
	}

}
