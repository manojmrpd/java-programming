package com.collection.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparatorExample {

	public static void main(String[] args) {
		
		List<Employee> list = new ArrayList<>();
		list.add(new Employee(1,"manoj",21, 20000));
		list.add(new Employee(2,"rakesh",25,50000));
		list.add(new Employee(3,"ashok",23, 40000));
		
		/*
		 * Sorting based on Salary
		 */
		SalaryComparator salary = new SalaryComparator();
		Collections.sort(list,salary);
		
		for(Employee e:list) {
			System.out.println(e.getEmpId()+" "+e.getEmpName()+" "+e.getAge()+ " "+e.getSalary());
		}
		
		/*
		 * Sorting based on Age
		 */
		AgeComparator age = new AgeComparator();
		Collections.sort(list, age);
		//Collections.sort(list, Employee.IdComparator);
		
		for(Employee e:list) {
			System.out.println(e.getEmpId()+" "+e.getEmpName()+" "+e.getAge()+ " "+e.getSalary());
		}
	}
}
