package com.java.oops.polymorphism;
//using Method overloading
class Helper {

	// Method with 2 integer parameters
	static int Multiply(int a, int b) {
		// Returns product of integer numbers
		return a * b;
	}

	// With same name but with 2 double parameters
	static double Multiply(double a, double b) {
		// Returns product of double numbers
		return a * b;
	}

	// Multiplication of 3 numbers
	static int Multiply(int a, int b, int c) {
		// Return product
		return a * b * c;
	}
}

public class CompileTimePolymorphism {

	public static void main(String[] args) {
		System.out.println(Helper.Multiply(2, 4));
		System.out.println(Helper.Multiply(2.5, 3.5));
		System.out.println(Helper.Multiply(2, 5, 3));
	}
}
