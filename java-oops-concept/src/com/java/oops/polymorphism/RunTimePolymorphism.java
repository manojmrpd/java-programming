package com.java.oops.polymorphism;
//Parent
class Parent {
	// Method of parent class
	void Print() {

		// Print statement
		System.out.println("parent class");
	}
}

// Sub class-1
class subclass1 extends Parent {

	// Method
	void Print() {
		System.out.println("subclass1");
	}
}

// Sub class-2
class subclass2 extends Parent {

	// Method
	void Print() {

		// Print statement
		System.out.println("subclass2");
	}
}

public class RunTimePolymorphism {

	public static void main(String[] args) {
		// Creating object of Parent
        Parent a;
 
        a = new subclass1();
        a.Print();
 
        a = new subclass2();
        a.Print();
	}

}
