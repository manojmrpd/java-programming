package com.java.oops.encapsulation;

class BankAccount {
	//private data members to hide the data
	private long accountNumber;
	private String name;
	private String email;
	private float balance;

	//public getter and setter methods 
	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
}

public class EncapsulationExample {

	public static void main(String[] args) {
		BankAccount account = new BankAccount();
		//setting values through setter methods 
		account.setName("Aaradhya");
		account.setEmail("acc@gmail.com");
		account.setAccountNumber(223494024);
		account.setBalance(145214);
	    //getting values through getter methods 
		System.out.println("The Name of account holder is " + account.getName() + " and Account number is: "
				+ account.getAccountNumber() + " and Email id is: " + account.getEmail() + " and Balance is: "
				+ account.getBalance());
	}
}
