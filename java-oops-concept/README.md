# Object Oriented Programming Principles

# 1. Class
A class is a user-defined blueprint or prototype from which objects are created. It represents the set of properties or methods that are common to all objects of one type. Using classes, you can create multiple objects with the same behavior instead of writing their code multiple times. This includes classes for objects occurring more than once in your code. In general, class declarations can include these components in order: 

* **Modifiers:** A class can be public or have default access (Refer to this for details).
* **Class name:** The class name should begin with the initial letter capitalized by convention.
* **Superclass (if any):** The name of the class’s parent (superclass), if any, preceded by the keyword extends. A class can only extend (subclass) one parent.
* **Interfaces (if any):** A comma-separated list of interfaces implemented by the class, if any, preceded by the keyword implements. A class can implement more than one interface.
* **Body:** The class body is surrounded by braces, { }.

# 2. Object
An object is a basic unit of Object-Oriented Programming that represents real-life entities. A typical Java program creates many objects, which as you know, interact by invoking methods. The objects are what perform your code, they are the part of your code visible to the viewer/user. An object mainly consists of: 

* **State:** It is represented by the attributes of an object. It also reflects the properties of an object.
* **Behavior:** It is represented by the methods of an object. It also reflects the response of an object to other objects.
* **Identity:** It is a unique name given to an object that enables it to interact with other objects.
* **Method:** A method is a collection of statements that perform some specific task and return the result to the caller. A method can perform some specific task without returning anything. Methods allow us to reuse the code without retyping it, which is why they are considered time savers. In Java, every method must be part of some class, which is different from languages like C, C++, and Python. 

# 3. Core OOPS principles

* Abstraction
* Encapsulation
* Inheritance
* Polymorphism
* Association
* Aggregation
* Composition

Let’s look into these object-oriented programming concepts one by one. We will use Java programming language for code examples so that you know how to implement OOPS concepts in Java.

## 1. Abstraction
Abstraction is the concept of hiding the internal details and describing things in simple terms. For example, a method that adds two integers. The internal processing of the method is hidden from the outer world. There are many ways to achieve abstraction in object-oriented programmings, such as encapsulation and inheritance.
### Program to illustrate Abstraction
```java
package com.java.oops.abstraction;

abstract class Shape {
	String color;

	// these are abstract methods
	abstract double area();

	public abstract String calculate();

	// abstract class can have the constructor
	public Shape(String color) {
		System.out.println("Shape constructor called");
		this.color = color;
	}

	// this is a concrete method
	public String getColor() {
		return color;
	}
}

class Circle extends Shape {
	double radius;

	public Circle(String color, double radius) {

		// calling Shape constructor
		super(color);
		System.out.println("Circle constructor called");
		this.radius = radius;
	}

	@Override
	double area() {
		return Math.PI * Math.pow(radius, 2);
	}

	@Override
	public String calculate() {
		return "Circle color is " + super.getColor() + " and area is : " + area();
	}
}

class Rectangle extends Shape {

	double length;
	double width;

	public Rectangle(String color, double length, double width) {
		// calling Shape constructor
		super(color);
		System.out.println("Rectangle constructor called");
		this.length = length;
		this.width = width;
	}

	@Override
	double area() {
		return length * width;
	}

	@Override
	public String calculate() {
		return "Rectangle color is " + super.getColor() + " and area is : " + area();
	}
}

public class AbstractionExample {

	public static void main(String[] args) {
		Shape s1 = new Circle("Red", 2.2);
        Shape s2 = new Rectangle("Yellow", 2, 4);
 
        System.out.println(s1.calculate());
        System.out.println(s2.calculate());
	}
}
```
### Realtime Example of Abstraction
* Your car is a great example of abstraction. You can start a car by turning the key or pressing the start button. You don’t need to know how the engine is getting started, what all components your car has. The car internal implementation and complex logic is completely hidden from the user.
* We can heat our food in Microwave. We press some buttons to set the timer and type of food. Finally, we get a hot and delicious meal. The microwave internal details are hidden from us. We have been given access to the functionality in a very simple manner.

## 2. Encapsulation
* It is defined as the wrapping up of data under a single unit. It is the mechanism that binds together the code and the data it manipulates. Another way to think about encapsulation is that it is a protective shield that prevents the data from being accessed by the code outside this shield. 
* Technically, in encapsulation, the variables or the data in a class is hidden from any other class and can be accessed only through any member function of the class in which they are declared.
* In encapsulation, the data in a class is hidden from other classes, which is similar to what data-hiding does. So, the terms “encapsulation” and “data-hiding” are used interchangeably.
* Encapsulation can be achieved by declaring all the variables in a class as private and writing public methods in the class to set and get the values of the variables.
* Encapsulation is the technique used to implement abstraction in object-oriented programming. Encapsulation is used for access restriction to class members and methods.

### Program to illustrate Encapsulation
```java
package com.java.oops.encapsulation;

class BankAccount {
	//private data members to hide the data
	private long accountNumber;
	private String name;
	private String email;
	private float balance;

	//public getter and setter methods 
	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
}

public class EncapsulationExample {

	public static void main(String[] args) {
		BankAccount account = new BankAccount();
		//setting values through setter methods 
		account.setName("Aaradhya");
		account.setEmail("acc@gmail.com");
		account.setAccountNumber(223494024);
		account.setBalance(145214);
	    //getting values through getter methods 
		System.out.println("The Name of account holder is " + account.getName() + " and Account number is: "
				+ account.getAccountNumber() + " and Email id is: " + account.getEmail() + " and Balance is: "
				+ account.getBalance());
	}
}
```
### Realtime example of Encapsulation
* Your school or office bag is great example of Encapsulation The bag contains different stuffs like pen, pencil, notebook etc within it, in order to get any stuff you need to open that bag, similarly in java an encapsulation unit contains it's data and behavior within it and in order to access them you need an object of that unit.

## 3. Inheritance
Inheritance is an important pillar of OOP (Object Oriented Programming). It is the mechanism in Java by which one class is allowed to inherit the features (fields and methods) of another class. 

Let us discuss some frequently used important terminologies:

### Superclass: 
The class whose features are inherited is known as superclass (also known as base or parent class).
### Subclass: 
The class that inherits the other class is known as subclass (also known as derived or extended or child class). The subclass can add its own fields and methods in addition to the superclass fields and methods.
### Reusability: 
Inheritance supports the concept of “reusability”, i.e. when we want to create a new class and there is already a class that includes some of the code that we want, we can derive our new class from the existing class. By doing this, we are reusing the fields and methods of the existing class.

### How to use inheritance in Java

The keyword used for inheritance is extends. 

#### Syntax : 

class derived-class extends base-class  
{  
   //methods and fields  
}  
*Example:* In the below example of inheritance, class Bicycle is a base class, class MountainBike is a derived class that extends Bicycle class and class Test is a driver class to run program. 
### Program to illustrate inheritance
```java
package com.java.oops.inheritance;

//base class
class Bicycle {
	// the Bicycle class has two fields
	public int gear;
	public int speed;

	// the Bicycle class has one constructor
	public Bicycle(int gear, int speed) {
		this.gear = gear;
		this.speed = speed;
	}

	// the Bicycle class has three methods
	public void applyBrake(int decrement) {
		speed -= decrement;
	}

	public void speedUp(int increment) {
		speed += increment;
	}

	// toString() method to print info of Bicycle
	public String toString() {
		return ("No of gears are " + gear + "\n" + "speed of bicycle is " + speed);
	}
}

//derived class
class MountainBike extends Bicycle {

	// the MountainBike subclass adds one more field
	public int seatHeight;

	// the MountainBike subclass has one constructor
	public MountainBike(int gear, int speed, int startHeight) {
		// invoking base-class(Bicycle) constructor
		super(gear, speed);
		seatHeight = startHeight;
	}

	// the MountainBike subclass adds one more method
	public void setHeight(int newValue) {
		seatHeight = newValue;
	}

	// overriding toString() method
	// of Bicycle to print more info
	@Override
	public String toString() {
		return (super.toString() + "\nseat height is " + seatHeight);
	}
}

public class InheritanceExample {

	public static void main(String[] args) {
		MountainBike mb = new MountainBike(3, 100, 25);
		mb.applyBrake(30);
        System.out.println("After Break Apply: \n===============\n"+mb.toString());
        mb.speedUp(30);
        System.out.println("After Speed Up: \n=============\n"+mb.toString());
	}
}
```
### Types of Inheritance
### 1. Single Inheritance: 
In Single inheritance, subclasses inherit the features of one superclass.
### 2.  Multilevel Inheritance: 
In Multilevel Inheritance, a derived class will be inheriting a base class and as well as the derived class also act as the base class to other class
### 3. Hierarchical Inheritance: 
In Hierarchical Inheritance, one class serves as a superclass (base class) for more than one subclass
### 4. Multiple Inheritance (Through Interfaces): 
In Multiple inheritances, one class can have more than one superclass and inherit features from all parent classes. Please note that Java does not support multiple inheritances with classes. In java, we can achieve multiple inheritances only through Interfaces. 

### Important facts about inheritance in Java 

* **Default superclass:** Except Object class, which has no superclass, every class has one and only one direct superclass (single inheritance). In the absence of any other explicit superclass, every class is implicitly a subclass of the Object class.
* **Superclass can only be one:** A superclass can have any number of subclasses. But a subclass can have only one superclass. This is because Java does not support multiple inheritances with classes. Although with interfaces, multiple inheritances are supported by java.
* **Inheriting Constructors:** A subclass inherits all the members (fields, methods, and nested classes) from its superclass. Constructors are not members, so they are not inherited by subclasses, but the constructor of the superclass can be invoked from the subclass.
* **Private member inheritance:** A subclass does not inherit the private members of its parent class. However, if the superclass has public or protected methods(like getters and setters) for accessing its private fields, these can also be used by the subclass.

## 4. Polymorphism
* It refers to the ability of object-oriented programming languages to differentiate between entities with the same name efficiently. This is done by Java with the help of the signature and declaration of these entities. 
* The word polymorphism means having many forms. In simple words, we can define polymorphism as the ability of a message to be displayed in more than one form. 

### Realtime Example of Polymorphism
A person at the same time can have different characteristics. Like a man at the same time is a father, a husband, an employee. So the same person possesses different behavior in different situations. This is called polymorphism. 

### Types of polymorphism
In Java polymorphism is mainly divided into two types: 

* Compile-time Polymorphism
* Runtime Polymorphism

### 1. Compile-time polymorphism
It is also known as static polymorphism. This type of polymorphism is achieved by method overloading. 
* **Method Overloading:** When there are multiple functions with the same name but different parameters then these functions are said to be overloaded. Functions can be overloaded by change in the number of arguments or/and a change in the type of arguments within the same class.

### Program to illustrate Compile time Polymorphism
```java
package com.java.oops.polymorphism;
//using Method overloading
class Helper {

	// Method with 2 integer parameters
	static int Multiply(int a, int b) {
		// Returns product of integer numbers
		return a * b;
	}

	// With same name but with 2 double parameters
	static double Multiply(double a, double b) {
		// Returns product of double numbers
		return a * b;
	}

	// Multiplication of 3 numbers
	static int Multiply(int a, int b, int c) {
		// Return product
		return a * b * c;
	}
}

public class CompileTimePolymorphism {

	public static void main(String[] args) {
		System.out.println(Helper.Multiply(2, 4));
		System.out.println(Helper.Multiply(2.5, 3.5));
		System.out.println(Helper.Multiply(2, 5, 3));
	}
}
```

### 2. Runtime Polymorphism
It is also known as Dynamic Method Dispatch. It is a process in which a function call to the overridden method is resolved at Runtime. This type of polymorphism is achieved by Method Overriding. 
* **Method overriding:** In method overriding, a subclass overrides a method with the same signature as that of in its superclass. During compile time, the check is made on the reference type. However, in the runtime, JVM figures out the object type and would run the method that belongs to that particular object.
### Program to illustrate Runtime Polymorphism
```java
package com.java.oops.polymorphism;
//Parent
class Parent {
	// Method of parent class
	void Print() {

		// Print statement
		System.out.println("parent class");
	}
}

// Sub class-1
class subclass1 extends Parent {

	// Method
	void Print() {
		System.out.println("subclass1");
	}
}

// Sub class-2
class subclass2 extends Parent {

	// Method
	void Print() {

		// Print statement
		System.out.println("subclass2");
	}
}

public class RunTimePolymorphism {

	public static void main(String[] args) {
		// Creating object of Parent
        Parent a;
 
        a = new subclass1();
        a.Print();
 
        a = new subclass2();
        a.Print();
	}

}

```
## 5. Association
Association is a relation between two separate classes which establishes through their Objects. Association can be one-to-one, one-to-many, many-to-one, many-to-many. In Object-Oriented programming, an Object communicates to another object to use functionality and services provided by that object. Composition and Aggregation are the two forms of association. 
### Example
Two separate classes Bank and Employee are associated through their Objects. Bank can have many employees, So it is a one-to-many relationship. 


## 6. Aggregation
It is a special form of Association where:  

* It represents **Has-A’s relationship**.
* It is a **unidirectional association** i.e. a one-way relationship. For example, a department can have students but vice versa is not possible and thus unidirectional in nature.
* In Aggregation, both the entries can survive individually which means ending one entity will not affect the other entity.

### Example
In this example, there is an Institute which has no. of departments like CSE, EE. Every department has no. of students. So, we make an Institute class that has a reference to Object or no. of Objects (i.e. List of Objects) of the Department class. That means Institute class is associated with Department class through its Object(s). And Department class has also a reference to Object or Objects (i.e. List of Objects) of the Student class means it is associated with the Student class through its Object(s). 

## 7. Composition 
Composition is a restricted form of Aggregation in which two entities are highly dependent on each other.  

* It represents **Part-of relationship**.
* In composition, both entities are dependent on each other.
* When there is a composition between two entities, the composed object cannot exist without the other entity.

### Example 
In this example, a library can have no. of books on the same or different subjects. So, If Library gets destroyed then All books within that particular library will be destroyed. i.e. books can not exist without libraries. That’s why it is composition.  Book is Part-of Library.

## Aggregation vs Composition
1. **Dependency:** Aggregation implies a relationship where the child can exist independently of the parent. For example, Bank and Employee, delete the Bank and the Employee still exist. whereas Composition implies a relationship where the child cannot exist independent of the parent. Example: Human and heart, heart don’t exist separate to a Human

2. **Type of Relationship:** Aggregation relation is “has-a” and composition is “part-of” relation.

3. **Type of association:** Composition is a strong Association whereas Aggregation is a weak Association.




