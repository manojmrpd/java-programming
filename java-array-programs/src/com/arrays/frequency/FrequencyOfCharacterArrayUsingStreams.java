package com.arrays.frequency;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FrequencyOfCharacterArrayUsingStreams {

	public static void main(String[] args) {
		String s = "bangalore";
		char[] arr = s.toCharArray();
		List<Character> list = new ArrayList<>();
		for(Character key:arr) {
			list.add(key);
		}
		Map<Character, Long> map = list.stream().collect(Collectors.groupingBy(p->p, Collectors.counting()));
		System.out.println(map);
		for (Character key : map.keySet()) {
            System.out.println(key + " occur " + map.get(key) + " time(s).");
        }

	}

}
