package com.arrays.frequency;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class FrequencyOfIntegerArrayUsingStreams {
	public static void main(String[] args) {

		int[] array = new int[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };
		Map<Integer, Long> map = Arrays.stream(array).boxed().collect(Collectors.groupingBy(Integer::intValue, Collectors.counting()));
		System.out.println(map);
		for (Integer key : map.keySet()) {
            System.out.println(key + " occur " + map.get(key) + " time(s).");
        }
	}
}
