package com.arrays.frequency;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FrequencyOfStringArrayUsingStreams {

	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("abc","xyz","abc","lmn","abc","xyz");
		Map<String, Long> map = list.stream().collect(Collectors.groupingBy(p->p, Collectors.counting()));
		System.out.println(map);
		for (String key : map.keySet()) {
            System.out.println(key + " occur " + map.get(key) + " time(s).");
        }
	}
}
