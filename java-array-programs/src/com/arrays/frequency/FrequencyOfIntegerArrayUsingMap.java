package com.arrays.frequency;
import java.util.HashMap;
import java.util.Map;

public class FrequencyOfIntegerArrayUsingMap {

	public static void main(String[] args) {
		int[] array = new int[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };
		Map<Integer, Integer> map = new HashMap<>();
        for (int key : array) {
            if (map.containsKey(key)) {
                map.put(key, map.get(key)+1);
            } else {
                map.put(key, 1);
            }
        }
        System.out.println(map);
        for (Integer key : map.keySet()) {
            System.out.println(key + " occur " + map.get(key) + " time(s).");
        }
	}
}
