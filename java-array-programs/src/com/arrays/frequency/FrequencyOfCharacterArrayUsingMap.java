package com.arrays.frequency;

import java.util.HashMap;
import java.util.Map;

public class FrequencyOfCharacterArrayUsingMap {
	
	public static void main(String[] args) {
		
		String s = "bangalore";
		char[] arr = s.toCharArray();
		Map<Character, Integer> map = new HashMap<>();
		for(char key: arr) {
			if(map.containsKey(key)) {
				map.put(key, map.get(key)+1);
			} else {
				map.put(key, 1);
			}
		}
		for(Character key: map.keySet()) {
			 System.out.println(key + " occur " + map.get(key) + " time(s).");
		}
	}

}
