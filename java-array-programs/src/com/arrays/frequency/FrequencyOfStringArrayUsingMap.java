package com.arrays.frequency;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FrequencyOfStringArrayUsingMap {
	
	public static void main(String[] args) {
		List<String> list = Arrays.asList("abc","xyz","abc","lmn","abc","xyz");
		Map<String, Integer> map = new HashMap<>();
		for(String key: list) {
			if(map.containsKey(key)) {
				map.put(key, map.get(key)+1);		
			} else {
				map.put(key, 1);
			}
		}
		System.out.println(map);
        for (String key : map.keySet()) {
            System.out.println(key + " occur " + map.get(key) + " time(s).");
        }
		
	}

}
