package com.arrays.sorting;

public class ReverseIntegerArray {

	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };
        System.out.println("Original array: ");  
		for (int i = 0; i < arr.length; i++) {  
            System.out.print(arr[i] + " ");  
        }  
		System.out.println("\n");
		System.out.println("Reverse array: ");  
		for(int i=arr.length-1;i>=0;i--) {
			System.out.print(arr[i] + " "); 
		}
	}
}
