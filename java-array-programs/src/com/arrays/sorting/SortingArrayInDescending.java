package com.arrays.sorting;

public class SortingArrayInDescending {
	
	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };

		Integer[] sortedArray = sortDescending(arr);
		for(int i=0;i<sortedArray.length;i++) {
			System.out.print(sortedArray[i]+",");
		}
	}

	private static Integer[] sortDescending(Integer[] array) {
		int temp = 0;
		for (int i = 0; i <= array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] < array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
		return array;
	}

}
