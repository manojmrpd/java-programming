package com.arrays.sorting;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SortingArrayUsingStreams {
	
	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };
		List<Integer> sortedList = Arrays.stream(arr).sorted().collect(Collectors.toList());
		System.out.println(sortedList);	
	}
}
