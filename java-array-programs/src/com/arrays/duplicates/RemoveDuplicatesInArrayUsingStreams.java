package com.arrays.duplicates;
import java.util.Arrays;

public class RemoveDuplicatesInArrayUsingStreams {

	public static void main(String[] args) {
		int[] array = new int[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };
		int[] removeDuplicates = Arrays.stream(array).sorted().distinct().toArray();
		for(int i:removeDuplicates) {
			System.out.print(" "+i);
		}
	}
}
