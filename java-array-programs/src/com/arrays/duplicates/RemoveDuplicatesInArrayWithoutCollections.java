package com.arrays.duplicates;

import java.util.Arrays;

public class RemoveDuplicatesInArrayWithoutCollections {
	
	public static void main(String[] args) {
		
		int[] array = new int[] { 1, 1, 3, 2, 1, 2, 3, 4, 5, 5, 3 };
		int[] unique = removeDuplicateElementsFromArray(array);
		for(int a: unique) {
			System.out.print(" "+a);
		}
	}

	private static int[] removeDuplicateElementsFromArray(int[] a) {
		
		Arrays.sort(a);
		int i=1,j=0;
		while(i<a.length) {
			if(a[i]==a[j]) {
				i++;
			} else {
				a[++j] = a[i++];
			}
		}
		int[] unique = Arrays.copyOf(a, j+1);
		return unique;
	}
}
