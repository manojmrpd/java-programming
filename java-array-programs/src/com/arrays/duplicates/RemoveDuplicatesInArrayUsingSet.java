package com.arrays.duplicates;

import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicatesInArrayUsingSet {

	public static void main(String[] args) {
		
		Integer[] duplicate = new Integer[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };
		Integer[] unique = removeDuplicatesUsingSet(duplicate);
		for(Integer i: unique) {
			System.out.print(" "+i);
		}
	}

	private static Integer[] removeDuplicatesUsingSet(Integer[] array) {
		Set<Integer> hashSet = new HashSet<>();
		for(Integer a: array) {
			hashSet.add(a);
		}
		Integer[] unique = hashSet.toArray(new Integer[hashSet.size()]);
		return unique;
	}
}
