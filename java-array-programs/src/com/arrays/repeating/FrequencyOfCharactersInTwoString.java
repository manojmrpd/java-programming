package com.arrays.repeating;

import java.util.HashMap;
import java.util.Map;

public class FrequencyOfCharactersInTwoString {

	public static void main(String[] args) {
		String s1="bangalore";
		String s2="hyderabad";
		String s3 = s1.concat(s2);
		char[] c = s3.toCharArray();
		Map<Character,Integer> map = new HashMap<>();
		for(Character key: c) {
			if(map.containsKey(key)){
				map.put(key, map.get(key)+1);
			} else {
				map.put(key, 1);
			}
		}
		
		for(Character key: map.keySet()) {
			System.out.println(key+" occurs "+map.get(key)+ " time(s)");
		}
	}
}
