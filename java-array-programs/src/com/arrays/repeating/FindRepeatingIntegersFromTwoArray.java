package com.arrays.repeating;

public class FindRepeatingIntegersFromTwoArray {

	public static void main(String[] args) {
		Integer[] a = new Integer[] { 1, 3, 4, 5 };
		Integer[] b = new Integer[] { 2, 1, 3 };
		System.out.print("Repeating elements from two arrays are:");
		for(int i=0;i<a.length;i++) {
			for(int j=0;j<b.length;j++) {
				if(a[i]==b[j]) {
					System.out.print(" "+a[i]);
				}
			}
		}
	}
}
