package com.arrays.repeating;

import java.util.LinkedHashMap;
import java.util.Map;

public class FindFirstNonRepeatingElementsUsingMap {
	
	public static void main(String[] args) {
		int[] array = new int[] { 1, 1, 3, 2, 1, 2, 3, 13, 10, 11, 5, 4 };
		Map<Integer, Integer> map = new LinkedHashMap<>();
        for (int key : array) {
            if (map.containsKey(key)) {
                map.put(key, map.get(key)+1);
            } else {
                map.put(key, 1);
            }
        }
        System.out.println(map);
        for (Integer key : map.keySet()) {
            if(map.get(key)==1) {
            	System.out.println("First Non Repeating Integer using LinkedHashMap is: "+key);
            	break;
            }
        }
	}

}
