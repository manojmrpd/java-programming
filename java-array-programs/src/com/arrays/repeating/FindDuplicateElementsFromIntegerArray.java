package com.arrays.repeating;

import java.util.HashMap;
import java.util.Map;

public class FindDuplicateElementsFromIntegerArray {

	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 1, 3, 2, 1, 2, 1, 2, 3, 4, 5 };
		Map<Integer, Integer> hm = new HashMap<>();
		for(Integer key: arr) {
			if(hm.containsKey(key)) {
				hm.put(key, hm.get(key)+1);
			} else {
				hm.put(key, 1);
			}
		}
		
		for(Integer key: hm.keySet()) {
			if(hm.get(key)>1) {
				System.out.println("Duplicate elements are: "+key);
			}
		}
	}

}
