package com.arrays.repeating;

public class FindTwoRepeatingElementsInArray {

	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 1, 3, 2, 1, 2, 3, 4, 5 };
		Integer[] duplicates = findTwoRepeatingElements(arr);
		System.out.println(duplicates);
	}

	private static Integer[] findTwoRepeatingElements(Integer[] arr) {
		System.out.print("Duplicates elements are: ");
		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++) {
				if(arr[i]==arr[j]) {
					System.out.print(" "+arr[j]);
				}		
			}
		}
		return arr;
	}
}
