	package com.arrays.repeating;
	
	import java.util.HashSet;
	
	public class FindTwoRepeatingElementsInArrayUsingSet {
	
		public static void main(String[] args) {
			Integer[] arr = new Integer[] { 1, 3, 2, 1, 2, 3, 4, 5 };
			HashSet<Integer> hs = new HashSet<>();
			System.out.print("Repeating/Duplicate elements are:");
			for(Integer a: arr) {
				if(!hs.add(a)) {
					System.out.print(" "+a);
				} 
			}
		}
	}
