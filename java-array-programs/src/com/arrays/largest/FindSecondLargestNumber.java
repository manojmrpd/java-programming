package com.arrays.largest;

import java.util.Arrays;

public class FindSecondLargestNumber {

	public static void main(String[] args) {
		
		int[] a = new int[] {1,1,2,3,3,2,4,4,5,4,1,1};
		int[] s = sortElements(a);
		for(int i:s) {
			System.out.print(" "+i);
		}
		System.out.println("\n");
		int[] u = removeDuplicates(s);
		System.out.println("Second largest number is "+u[u.length-2]);
		for(int i:u) {
			System.out.print(" "+i);
		}
	}

	private static int[] removeDuplicates(int[] a) {
		
		int i=1,j=0;
		while(i<a.length) {
			if(a[i]==a[j]) {
				i++;
			} else {
				a[++j] = a[i++];
			}
		}
		int[] arr = Arrays.copyOf(a, j+1);
		
		return arr;
	}

	private static int[] sortElements(int[] a) {
		
		int temp = 0;
		for(int i=0;i<a.length;i++) {
			for(int j=i+1;j<a.length;j++) {
				if(a[i]>a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		return a;
	}

}
