package com.arrays.largest;

import java.util.Arrays;

public class LargestNumberInDuplicateArrayWithoutCollection {

	public static void main(String[] args) {
		
		int[] array = new int[] { 1, 1, 3, 3, 2, 2, 5, 5, 4, 4 };
		//First Sort the elements
		int[] sorted = sortElements(array);
		//Second Remove duplicate elements
		int[] max = removeDuplicates(sorted);
		System.out.println("First largest number is: "+max[0]);
		System.out.println("Second largest number is: "+max[1]);
		System.out.println("Third largest number is: "+max[2]);		
	}
	
	//sorting elements in an array
	private static int[] sortElements(int[] arr) {
		int temp=0;
		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++) {
				if(arr[i]<arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		return arr;
	}
	
	//remove duplicates elements in an sorted array
	private static int[] removeDuplicates(int[] arr) {
		int i=1,j=0;
		while(i<arr.length){
			if(arr[i]==arr[j]) {
				i++;
			} else {
				arr[++j] = arr[i++];
			}
		}
		int[] ar = Arrays.copyOf(arr, j+1);
		return ar;
	}
}
