package com.arrays.largest;
/*
 * Using Stream API - sorted() and distinct()
 */
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LargestNumberInArrayUsingStreams {
	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 1, 1, 3, 2, 1, 2, 3, 4, 5 };
		List<Integer> list = Arrays.stream(arr).distinct().sorted().collect(Collectors.toList());
		System.out.println("The First largest number is: "+list.get(list.size()-1));
		System.out.println("The Second largest number is: "+list.get(list.size()-2));
		System.out.println("The Second largest number is: "+list.get(list.size()-3));
	}
}
