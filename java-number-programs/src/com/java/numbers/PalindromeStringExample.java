package com.java.numbers;
/*
 * Reverse of a given number should be equal.
 */

import java.util.Scanner;

public class PalindromeStringExample {
	
	private static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		System.out.println("Please enter a string: ");
		String str = scanner.next();
		System.out.println("Original string: "+str);
		String rev="";
		for(int i=str.length()-1;i>=0;i--) {
			rev = rev+str.charAt(i);
		}
		System.out.println("Reverse string: "+rev);
		if(str.equals(rev)) {
			System.out.println(str+" Is a Palindrome string");
		} else {
			System.out.println(str+" Is Not a Palindrome string");
		}
	}
}
