package com.java.numbers;

/*
 * WAP to print composite number from 1-100
 * What is Composite number?
 * A Composite number is the one which is multiply by 2 factor
 */

public class CompositeNumberExample {

	public static void main(String[] args) {
		for(int i=2; i<=100; i++) {
			boolean isPrime = true;
			for(int j=2; j < i; j++) {
				if(i%j == 0) {
					isPrime = false;
					break;
				}
			}
			if(!isPrime) {
				System.out.print(i+ " , ");
			}
		}
	}
}
