package com.java.numbers;

import java.util.Scanner;

public class EvenOddExample {

	private static Scanner scanner;

	public static void main(String[] args) {

		scanner = new Scanner(System.in);
		System.out.println("Enter num1: ");
		int num = scanner.nextInt();
		if (num % 2 == 0) {
			System.out.println(num + " is a Even number");
		} else {
			System.out.println(num + " is a Odd number");
		}
	}
}
