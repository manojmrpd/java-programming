package com.java.numbers;

/*
 * WAP to print prime number from 1-100
 * A Prime number is the one which is multiply by 1 but cannot be multiply by two different numbers
 * 5 --> 1 x 5 or 5 x 1 (2,3,4)
 * 7 --> 1 x 7 or 7 x 1 (2,3,4,5,6)
 */
public class PrimeNumberExample {

	public static void main(String[] args) {
		for(int i=2; i<=100; i++) {
			boolean isPrime = true;
			for(int j=2; j < i; j++) {
				if(i%j == 0) {
					isPrime = false;
					break;
				}
			}
			if(isPrime) {
				System.out.print(i+ " , ");
			}
		}
	}
}
