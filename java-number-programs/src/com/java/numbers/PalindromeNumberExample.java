package com.java.numbers;
/*
 * Palindrome number are nothing but the reverse of a given number should be equal to original number
 * Example - 121, 111, 191 
 */

import java.util.Scanner;

public class PalindromeNumberExample {

	private static Scanner scanner;

	public static void main(String[] args) {

		scanner = new Scanner(System.in);
		System.out.println("Enter num1: ");
		int num = scanner.nextInt();
		int temp = num;
		int r, sum = 0;
		while (num > 0) {
			r = num % 10;
			sum = (sum * 10) + r;
			num = num / 10;
		}
		if(temp == sum) {
			System.out.println("Palindrome number");
		} else {
			System.out.println("Not a Palindrome Number");
		}
	}
}
