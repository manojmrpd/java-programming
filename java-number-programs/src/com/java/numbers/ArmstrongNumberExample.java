package com.java.numbers;

import java.util.Scanner;
/*
 * Armstrong number example 153 = 1power3 + 5power3  + 3power3 = 153
 */

public class ArmstrongNumberExample {

	private static Scanner scanner;

	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		System.out.println("Enter number: ");
		int num = scanner.nextInt();
		int temp = num;
		int r, sum = 0;
		while (num > 0) {
			r = num % 10;
			sum = sum+ (r*r*r*r);
			num = num / 10;
		}
		if(temp == sum) {
			System.out.println("Armstrong number");
		} else {
			System.out.println("Not a Armstrong Number");
		}
		

	}

}
