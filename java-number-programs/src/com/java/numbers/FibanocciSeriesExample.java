package com.java.numbers;
/*
 * Fibanocci number 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144
 */

import java.util.Scanner;

public class FibanocciSeriesExample {

	private static Scanner scanner;

	public static void main(String[] args) {

		scanner = new Scanner(System.in);
		System.out.println("Enter num1: ");
		int num1 = scanner.nextInt();
		System.out.println("Enter num2");
		int num2 = scanner.nextInt();

		int num3 = 0;
		for (int i = 0; i <= 10; i++) {
			num3 = num1 + num2;
			num1 = num2;
			num2 = num3;
			System.out.print(num3 + ", ");
		}
	}
}
