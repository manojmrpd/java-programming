package com.java.numbers;

/*
 *WAP to Enter a number and check whether the number is prime or composite?
 */

import java.util.Scanner;

public class PrimeOrCompositeExample {

	private static Scanner scanner;

	public static void main(String[] args) {

		scanner = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int num = scanner.nextInt();
		boolean isPrime = isPrime(num);
		if (isPrime) {
			System.out.println(num + " is a prime number");
		} else {
			System.out.println(num + " is a composite number");
		}
	}

	private static boolean isPrime(int num) {
		boolean flag = true;
		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				flag = false;
				break;
			}
		}
		return flag;
	}
}
