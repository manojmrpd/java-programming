package com.java.finalkeyword.classes;

public class FinalClassExample {

}

final class A {
	void m1() {
		System.out.println("m1 method");
	}
}
// Compile time error.
//Final Class cannot be inherited
//class B extends A {
//}
