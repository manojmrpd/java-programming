package com.java.finalkeyword.variables;

public class FinalVariableObservation1 {

	// Declaring and customly initializing
	// static final variable
	static final int CAPACITY = 4;

	// Main driver method
	public static void main(String args[]) {

		// Re-assigning final variable
		// will throw compile-time error
		//CAPACITY = 5;
	}

}
