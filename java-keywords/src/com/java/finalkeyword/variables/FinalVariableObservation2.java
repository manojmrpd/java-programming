package com.java.finalkeyword.variables;

public class FinalVariableObservation2 {
	public static void main(String[] args) {
		// Creating an object of StringBuilder class
		// Final reference variable
		final StringBuilder sb = new StringBuilder("Java");

		// Printing the element in StringBuilder object
		System.out.println(sb);

		// changing internal state of object reference by
		// final reference variable sb
		sb.append("Programming");

		// Again printing the element in StringBuilder
		// object after appending above element in it
		System.out.println(sb);
	}

}
