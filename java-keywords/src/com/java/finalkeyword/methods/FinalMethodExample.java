package com.java.finalkeyword.methods;

public class FinalMethodExample {

	public static void main(String[] args) {
	}

}

class A {
	final void m1() {
		System.out.println("m1 method");
	}
}
class B extends A {
	// Compile time error 
	//Final method cannot be ovverriden
	//void m1() 
	{
		
	}
}