package com.java.volatilekeyword;

import java.util.Scanner;

class Test extends Thread {
	boolean running = true;

	@Override
	public void run() {
		while (running) {
			System.out.println("GeeksforGeeks");
		}
	}

	public void shutDown() {
		running = false;
	}
}

public class NonVolatileExample {
	private static Scanner input;

	public static void main(String[] args) {
		Test obj = new Test();
		obj.start();
		input = new Scanner(System.in);
		input.nextLine();
		obj.shutDown();
	}
}
