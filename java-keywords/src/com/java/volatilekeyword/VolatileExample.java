package com.java.volatilekeyword;

import java.util.Scanner;

class Example extends Thread {
	// volatile variable
	volatile boolean running = true;

	@Override
	public void run() {
		while (running) {
			System.out.println("GeeksforGeeks");
		}
	}

	public void shutDown() {
		running = false;
	}
}

public class VolatileExample {
	private static Scanner input;

	public static void main(String[] args) {
		Example obj = new Example();
		obj.start();
		input = new Scanner(System.in);
		input.nextLine();
		obj.shutDown();
	}
}
