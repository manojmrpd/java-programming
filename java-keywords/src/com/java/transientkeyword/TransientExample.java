package com.java.transientkeyword;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// normal variable
	int age = 30;

	// Transient variables
	transient String userId = "admin";
	transient String password = "tiger123";
}

public class TransientExample {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		User user = new User();
		System.out.println("Age is: " + user.age);
		System.out.println("User ID: " + user.userId);
		System.out.println("Password: " + user.password);

		// serialization
		FileOutputStream fos = new FileOutputStream("abc.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(user);

		// de-serialization
		FileInputStream fis = new FileInputStream("abc.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		User userData = (User) ois.readObject();

		// printing the value of transient
		// variable after de-serialization process
		System.out.println("Age: " + userData.age);
		System.out.println("User ID :" + userData.userId);
		System.out.println("Password: " + userData.password);

	}
}
