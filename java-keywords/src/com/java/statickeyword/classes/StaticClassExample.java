package com.java.statickeyword.classes;

public class StaticClassExample {
	
	private static String str = "GeeksforGeeks";
	String s = "Test";
	  
    // Static class
    static class MyNestedClass {
        
        // non-static method
        public void display(){ 
          System.out.println(str); 
          //System.out.println(s); // Compile error
        }
    }
    
    public static void main(String args[])
    {
    	StaticClassExample.MyNestedClass obj
            = new StaticClassExample.MyNestedClass();
        obj.display();
    }
}
