package com.java.statickeyword.blocks;

class Student {
	// declare the variable as static
    protected static String studentName = "Manoj"; 
}

public class StaticExample {

	public static void main(String[] args) {
		System.out.println("The student name is : "+Student.studentName);
	}
}
