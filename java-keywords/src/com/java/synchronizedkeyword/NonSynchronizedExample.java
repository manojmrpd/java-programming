package com.java.synchronizedkeyword;

class Counter {
	int count;

	void increment() {
		count++;
	}
}

public class NonSynchronizedExample {

	public static void main(String[] args) throws InterruptedException {
		Counter c = new Counter();

		// Thread 1
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		// Thread 2
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		t1.start();
		t2.start();
		t1.join();
		t2.join();

		System.out.println("The count in the non-synchorized code is: " + c.count);
	}

}
