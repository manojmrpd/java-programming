package com.java.synchronizedkeyword;

class Counters {
	int count;

	//make synchronized method
	synchronized void increment() {
		count++;
	}
}

public class SynchronizedExample {

	public static void main(String[] args) throws InterruptedException {
		Counters c = new Counters();

		// Thread 1
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		// Thread 2
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		t1.start();
		t2.start();
		t1.join();
		t2.join();
		
		System.out.println("The count in the non-synchronized code is: " + c.count);
	}

}
