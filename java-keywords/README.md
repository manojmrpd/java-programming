# Access Modifiers

Access Modifiers in Java help restrict the scope of a variable, method, class, or constructor. Public, Private, Protected, and Default these four access modifiers are present in Java.
* public
* private
* default
* protected

## 1. public 
When a member of a class is modified by public, then that member can be accessed by any other code.
## 2. private
 When a member of a class is specified as private, then that member can only be accessed by other members of its class. 
## 3. default
It is also referred to as no modifier. Whenever we do not use any access modifier it is treated as default where this allows us to access within a class, within a subclass, and also non-sub class within a package but when the package differs now be it a subclass or non-class we are not able to access. 
## 4. protected 
With the above default keyword we were facing an issue as we are getting closer to the real world with the above default modifier but there was a constriction as we are not able to access class sub-class from a different package. So protected access modifier allows not only to access class be it subclass or non-sub class but allows us to access subclass of the different package which brings us very close to a real-world and hence strong understanding of inheritance is required for understanding and implementing this keyword. 
## Understanding Access modifiers 
![image](https://media.geeksforgeeks.org/wp-content/cdn-uploads/Access-Modifiers-in-Java.png)

There are four access modifiers keywords in Java and they are:

| Modifier     | Description    |
| ------------- | ------------- | 
| Default          | declarations are visible only within the package (package private)         | 
| Private           | declarations are visible within the class only         | 
| Protected           | declarations are visible within the package or all subclasses         | 
| Public           | declarations are visible everywhere         | 

# Non-Access Modifiers
Non-access modifiers provide information about the characteristics of a class, method, or variable to the JVM. Seven types of Non-Access modifiers are present in Java. They are –

* static
* final
* abstract
* synchronized
* volatile
* transient
* native

## 1. static
The static keyword means that the entity to which it is applied is available outside any particular instance of the class. That means the static methods or the attributes are a part of the class and not an object. The memory is allocated to such an attribute or method at the time of class loading. The use of a static modifier makes the program more efficient by saving memory. A static field exists across all the class instances, and without creating an object of the class, they can be called.
### Program to illustrate static keyword
```java
package com.java.accessmodifiers.statics;

class Student {
	// declare the variable as static
    protected static String studentName = "Manoj"; 
}

public class StaticExample {

	public static void main(String[] args) {
		System.out.println("The student name is : "+Student.studentName);
	}
}
```
### Output 
```bash
The student name is: Manoj
```
In this above code sample, we have declared the String as static, part of the Student class. Generally, to access the string, we first need to create the object of the Student class, but as we have declared it as static, we do not need to create an object of Student class to access the string. We can use className.variableName for accessing it.
When a member is declared static, it can be accessed before any objects of its class are created, and without reference to any object.

### Types of static keyword
The static keyword is a non-access modifier in Java that is applicable for the following: 
* Blocks
* Variables
* Methods
* Classes
### 1.1 Static blocks
If you need to do the computation in order to initialize your static variables, you can declare a static block that gets executed exactly once, when the class is first loaded.
### Program to illustrate static block
```java

class StaticBlockExample
{
	// static variable
	static int a = 10;
	static int b;
	
	// static block
	static {
		System.out.println("Static block initialized.");
		b = a * 4;
	}

	public static void main(String[] args)
	{
	System.out.println("from main");
	System.out.println("Value of a : "+a);
	System.out.println("Value of b : "+b);
	}
}
```
### Output
```bash
Static block initialized.
from main
Value of a : 10
Value of b : 40
```
### 1.2 Static variables
When a variable is declared as static, then a single copy of the variable is created and shared among all objects at the class level. Static variables are, essentially, global variables. All instances of the class share the same static variable.

### Important points for static variables:

* We can create static variables at the class level only. See here
* static block and static variables are executed in the order they are present in a program.
Below is the Java program to demonstrate that static block and static variables are executed in the order they are present in a program. 
### Java program to demonstrate static variables
```java
// of static blocks and variables

class StaticVariableExample
{
	// static variable
	static int a = m1();
	
	// static block
	static {
		System.out.println("Inside static block");
	}
	
	// static method
	static int m1() {
		System.out.println("from m1");
		return 20;
	}
	
	// static method(main !!)
	public static void main(String[] args)
	{
	System.out.println("Value of a : "+a);
	System.out.println("from main");
	}
}
```
### 1.3 Static methods
When a method is declared with the static keyword, it is known as the static method. The most common example of a static method is the main( ) method. As discussed above, Any static member can be accessed before any objects of its class are created, and without reference to any object. Methods declared as static have several restrictions: 

* They can only directly call other static methods.
* They can only directly access static data.
* They cannot refer to this or super in any way.
Below is the java program to demonstrate restrictions on static methods.
### Program to illustrate static methods
```java
package com.keyword.statickeyword.methods;

public class StaticMethodExample {

	// static variable
	static int a = 10;

	// instance variable
	int b = 20;

	// static method
	static void m1() {
		a = 20;
		System.out.println("from m1");

		// Cannot make a static reference to the non-static field b
		b = 10; // compilation error

		// Cannot make a static reference to the
		// non-static method m2() from the type Test
		m2(); // compilation error

		// Cannot use this or super in a static context
		System.out.println(super.a); // compiler error
        System.out.println(this.a); // compile error
	}

	// instance method
	void m2() {
		System.out.println("from m2");
	}
}
```
### 1.4 Static Classes
A class can be made static only if it is a nested class. We cannot declare a top-level class with a static modifier but can declare nested classes as static. Such types of classes are called Nested static classes. Nested static class doesn’t need a reference of Outer class. In this case, a static class cannot access non-static members of the Outer class. 
### Program to illustrate static classes
```java
package com.java.statickeyword.classes;

package com.java.statickeyword.classes;

public class StaticClassExample {
	
	private static String str = "GeeksforGeeks";
    // instance variable
	String s = "Test";
	  
    // Static class
    static class MyNestedClass {
        
        // non-static method
        public void display(){ 
          System.out.println(str); 
          System.out.println(s); // Compile error
        }
    }
    
    public static void main(String args[])
    {
    	StaticClassExample.MyNestedClass obj
            = new StaticClassExample.MyNestedClass();
        obj.display();
    }
}
```
### When to use static variables and methods?
Use the static variable for the property that is common to all objects. For example, in class Student, all students share the same college name. Use static methods for changing static variables.
![image](https://media.geeksforgeeks.org/wp-content/cdn-uploads/d.jpeg)

## 2. final
final keyword is used in different contexts. First of all, final is a non-access modifier applicable only to a variable, a method, or a class. The following are different contexts where final is used.

![image](https://media.geeksforgeeks.org/wp-content/uploads/20220225104709/FinalkeywordinJava.jpg)

### 2.1 Final Variables
When a variable is declared with the final keyword, its value can’t be modified, essentially, a constant. This also means that you must initialize a final variable. If the final variable is a reference, this means that the variable cannot be re-bound to reference another object, but the internal state of the object pointed by that reference variable can be changed i.e. you can add or remove elements from the final array or final collection. It is good practice to represent final variables in all uppercase, using underscore to separate words.

### Initializing a final Variable
We must initialize a final variable, otherwise, the compiler will throw a compile-time error. A final variable can only be initialized once, either via an initializer or an assignment statement. There are three ways to initialize a final variable: 

* You can initialize a final variable when it is declared. This approach is the most common. A final variable is called a blank final variable if it is not initialized while declaration. Below are the two ways to initialize a blank final variable.
* A blank final variable can be initialized inside an instance-initializer block or inside the constructor. If you have more than one constructor in your class then it must be initialized in all of them, otherwise, a compile-time error will be thrown.
* A blank final static variable can be initialized inside a static block.
### Program to illustrate final variable
```java
package com.java.finalkeyword.variables;

public class FinalVariableExample {

	// a final variable
	// direct initialize
	final int THRESHOLD = 5;

	// a blank final variable
	final int CAPACITY;

	// another blank final variable
	final int MINIMUM;

	// a final static variable PI
	// direct initialize
	static final double PI = 3.141592653589793;

	// a blank final static variable
	static final double EULERCONSTANT;

	// instance initializer block for
	// initializing CAPACITY
	{
		CAPACITY = 25;
	}

	// static initializer block for
	// initializing EULERCONSTANT
	static {
		EULERCONSTANT = 2.3;
	}

	// constructor for initializing MINIMUM
	// Note that if there are more than one
	// constructor, you must initialize MINIMUM
	// in them also
	public FinalVariableExample() 
    {
        MINIMUM = -1;
    }

}
```
### Observation 1: Final variable cannot be re-assigned

The only difference between a normal variable and a final variable is that we can re-assign the value to a normal variable but we cannot change the value of a final variable once assigned. Hence final variables must be used only for the values that we want to remain constant throughout the execution of the program.

### Program to illustrate Observation-1
```java
package com.java.finalkeyword.variables;

public class FinalVariableObservation1 {

	// Declaring and customly initializing
	// static final variable
	static final int CAPACITY = 4;

	// Main driver method
	public static void main(String args[]) {

		// Re-assigning final variable
		// will throw compile-time error
		CAPACITY = 5;
	}
}
```

### Observation 2: Reference final variable state can be changed

When a final variable is a reference to an object, then this final variable is called the reference final variable. For example, a final StringBuffer variable looks defined below as follows:
```bash
final StringBuffer sb;
```
As we all know that a final variable cannot be re-assign. But in the case of a reference final variable, the internal state of the object pointed by that reference variable can be changed. Note that this is not re-assigning. This property of final is called non-transitivity. To understand what is meant by the internal state of the object as shown in the below example as follows:
### Program to illustrate Observation-2
```java
package com.java.finalkeyword.variables;

public class FinalVariableExample2 {
	public static void main(String[] args) {
		// Creating an object of StringBuilder class
		// Final reference variable
		final StringBuilder sb = new StringBuilder("Java");

		// Printing the element in StringBuilder object
		System.out.println(sb);

		// changing internal state of object reference by
		// final reference variable sb
		sb.append("Programming");

		// Again printing the element in StringBuilder
		// object after appending above element in it
		System.out.println(sb);
	}
}
```
Output
```bash
Java
JavaProgramming
```
The non-transitivity property also applies to arrays, because arrays are objects in Java. Arrays with the final keyword are also called final arrays.

### 2.2  Final classes
When a class is declared with final keyword, it is called a final class. A final class cannot be extended(inherited). 

There are two uses of a final class: 

**Usage 1:** One is definitely to prevent inheritance, as final classes cannot be extended. For example, all Wrapper Classes like Integer, Float, etc. are final classes. We can not extend them.
```java
final class A
{
     // methods and fields
}
// The following class is illegal
class B extends A 
{ 
    // COMPILE-ERROR! Can't subclass A
}
```
**Usage 2:** The other use of final with classes is to create an immutable class like the predefined String class. One can not make a class immutable without making it final.

### 2.3 Final methods

When a method is declared with final keyword, it is called a final method. A final method cannot be overridden. The Object class does this—a number of its methods are final. We must declare methods with the final keyword for which we are required to follow the same implementation throughout all the derived classes. 

**Illustration:** Final keyword with a method 

```java
class A 
{
    final void m1() 
    {
        System.out.println("This is a final method.");
    }
}

class B extends A 
{
    void m1()
    { 
        // Compile-error! We can not override
        System.out.println("Illegal!");
    }
}
```

## 3. abstract
abstract is a non-access modifier in java applicable for classes, methods but not variables. It is used to achieve abstraction which is one of the pillar of Object Oriented Programming(OOP). Following are different contexts where abstract can be used in Java.
### Types of abstract
* abstract classes
* abstract methods

### 3.1 Abstract classes
The class which is having partial implementation(i.e. not all methods present in the class have method definition). To declare a class abstract, use this general form : 
```java
abstract class class-name{
    //body of class
}
```
Due to their partial implementation, we cannot instantiate abstract classes.Any subclass of an abstract class must either implement all of the abstract methods in the super-class, or be declared abstract itself.Some of the predefined classes in java are abstract. They depend on their sub-classes to provide complete implementation. For example, java.lang.Number is a abstract class. For more on abstract classes, see abstract classes in java

### 3.2 Abstract methods
Sometimes, we require just method declaration in super-classes.This can be achieve by specifying the abstract type modifier. These methods are sometimes referred to as subclasser responsibility because they have no implementation specified in the super-class. Thus, a subclass must override them to provide method definition. To declare an abstract method, use this general form: 
```java
abstract type method-name(parameter-list);
```
As you can see, no method body is present.Any concrete class(i.e. class without abstract keyword) that extends an abstract class must overrides all the abstract methods of the class.


### Program to illustrate abstract keyword
```java
package com.java.abstractkeyword;

public class AbstractExample {

	public static void main(String args[]) {
		B b = new B();
		b.m1();
		b.m2();
	}
}

//abstract with class
abstract class A {
	// abstract with method
	// it has no body
	abstract void m1();

	// concrete methods are still allowed in abstract classes
	void m2() {
		System.out.println("This is a concrete method.");
	}
}

//concrete class B
class B extends A {
	// class B must override m1() method
	// otherwise, compile-time exception will be thrown
	void m1() {
		System.out.println("B's implementation of m1.");
	}
}
```
Output
```bash
B's implementation of m1.
This is a concrete method.
```
### Important rules for abstract keyword
* Any class that contains one or more abstract methods must also be declared abstract
* The following are various illegal combinations of other modifiers for methods with respect to abstract modifier : 
    * abstract final
    * abstract native
    * abstract synchronized
    * abstract static
    * abstract private
    * abstract strictfp
* Although abstract classes cannot be used to instantiate objects, they can be used to create object references, because Java’s approach to run-time polymorphism is implemented through the use of super-class references. Thus, it must be possible to create a reference to an abstract class so that it can be used to point to a subclass object.
* In java, you will never see a class or method declared with both final and abstract keywords. For classes, final is used to prevent inheritance whereas abstract classes depends upon their child classes for complete implementation. In cases of methods, final is used to prevent overriding whereas abstract methods needs to be overridden in sub-classes.

## 4. synchronized
synchronized keyword prevents a block of code from executing by multiple threads at once. It is very important for some critical operations. Let us understand by an example –

### Program to illustrate non-synchronized code in multithreading
```java
package com.java.synchronizedkeyword;

class Counter {
	int count;

	void increment() {
		count++;
	}
}

public class SynchronizedExample {

	public static void main(String[] args) throws InterruptedException {
		Counter c = new Counter();

		// Thread 1
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		// Thread 2
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		t1.start();
		t2.start();
		t1.join();
		t2.join();
		
        System.out.println("The count in the non-synchorized code is: " + c.count);
	}
}
```
Output
```bash
The count in the non-synchorized code is: 1958
```
### Observations on non-synchronized in multihreading
* The above code should be an output value of 2000 as two threads increment it 1000 times each, and the main is waiting for Thread1, Thread2 to finish. Sometimes it may not be true. Depending upon the system, it may not give 2000 as output.  
* As both threads are accessing the value of count, it may happen that Thread1 fetches the value of count, and before it could increment it, the Thread2 reads the value and increments that. So thus, the result may be less than 2000.  

### Program to illustrate synchronized code in multithreading 

To solve this issue, we use the synchronized keyword. If the synchronized keyword is used while declaring the increment() method, then a thread needs to wait for another thread to complete the operation of the method then only another one can work on it. So we can get guaranteed output of 2000.
```java
package com.java.synchronizedkeyword;

class Counters {
	int count;

	//make synchronized method
	synchronized void increment() {
		count++;
	}
}

public class SynchronizedExample {

	public static void main(String[] args) throws InterruptedException {
		Counters c = new Counters();

		// Thread 1
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		// Thread 2
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					c.increment();
				}
			}
		});

		t1.start();
		t2.start();
		t1.join();
		t2.join();
		
        System.out.println(c.count);
	}
}
```
Output
```bash
The count in the non-synchronized code is: 2000
```

## 5. volatile
* The volatile keyword is used to make the class thread-safe. That means if a variable is declared as volatile, then that can be modified by multiple threads at the same time without any issues. 
* The volatile keyword is only applicable to a variable. 
* A volatile keyword reduces the chance of memory inconsistency. 
* The value of a volatile variable is always read from the main memory and not from the local thread cache, and it helps to improve thread performance. 

Let us understand by an example:
### Program to illustrate non-volatile:
```java
package com.java.volatilekeyword;

import java.util.Scanner;

class Test extends Thread {
	boolean running = true;

	@Override
	public void run() {
		while (running) {
			System.out.println("GeeksforGeeks");
		}
	}

	public void shutDown() {
		running = false;
	}
}

public class NonVolatileExample {
	private static Scanner input;

	public static void main(String[] args) {
		Test obj = new Test();
		obj.start();
		input = new Scanner(System.in);
		input.nextLine();
		obj.shutDown();
	}
}
```
Output:
![image](https://media.geeksforgeeks.org/wp-content/uploads/20211118111617/error5-660x308.jpg)
### Observation in non-volatile
In the above code, the program should ideally stop if Return Key/Enter is pressed, but in some machines, it may happen that the variable running is cached, and we are not able to change its value using the shutDown() method. In such a case, the program will execute infinite times and cannot be exited properly. To avoid caching and make it Thread-safe, we can use volatile keywords while declaring the running variable.
### Program to illustrate volatile
```java
package com.java.volatilekeyword;

import java.util.Scanner;

class Example extends Thread {
	// volatile variable
	volatile boolean running = true;

	@Override
	public void run() {
		while (running) {
			System.out.println("GeeksforGeeks");
		}
	}

	public void shutDown() {
		running = false;
	}
}

public class VolatileExample {
	private static Scanner input;

	public static void main(String[] args) {
		Example obj = new Example();
		obj.start();
		input = new Scanner(System.in);
		input.nextLine();
		obj.shutDown();
	}
}
```
**Output**
![image](https://media.geeksforgeeks.org/wp-content/uploads/20211118111925/output5-660x382.jpg)
### Observation
In the above code, after using the volatile keyword, we can stop the infinite loop using the Return key, and the program exited properly with exit code 0.

## 6. transient
* The transient keyword may be applied to member variables of a class to indicate that the member variable should not be serialized when the containing class instance is serialized. 
* Serialization is the ​process of converting an object into a byte stream. When we do not want to serialize the value of a variable, then we declare it as transient. 
* To make it more transparent, let’s take an example of an application where we need to accept UserID and Password. At that moment, we need to declare some variable to take the input and store the data, but as the data is susceptible, so we do not want to keep it stored after the job is done. 
* To achieve this, we can use the transient keyword for variable declaration. That particular variable will not participate in the serialization process, and when we deserialize that, we will receive the default value of the variable. Let’s see a sample code for the same –

### Program to illustrate transient
```java
package com.java.transientkeyword;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// normal variable
	int age = 30;

	// Transient variables
	transient String userId = "admin";
	transient String password = "tiger123";
}

public class TransientExample {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		User user = new User();
		System.out.println("Age is: " + user.age);
		System.out.println("User ID: " + user.userId);
		System.out.println("Password: " + user.password);

		// serialization
		FileOutputStream fos = new FileOutputStream("abc.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(user);

		// de-serialization
		FileInputStream fis = new FileInputStream("abc.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		User userData = (User) ois.readObject();

		// printing the value of transient
		// variable after de-serialization process
		System.out.println("Age: " + userData.age);
		System.out.println("User ID :" + userData.userId);
		System.out.println("Password: " + userData.password);

	}
}
```
Output
```bash
Age is: 30
User ID: admin
Password: tiger123
Age: 30
User ID :null
Password: null
```
As you see from the output, after serialization, the values of UserID and Password are no longer present. However, the value of ‘age’, which is a normal variable, is still present.

## 7. native
* The native keyword in Java is applied to a method to indicate that the method is implemented in native code using JNI (Java Native Interface). The native keyword is a modifier that is applicable only for methods, and we can’t apply it anywhere else. The methods which are implemented in C, C++ are called native methods or foreign methods.
* Using this java application can call code written in C, C++, or assembler language.
* A shared code library or DLL is required in this case. Let’s see an example first –

### Program to illustrate native
```java
import java.io.*;

class NativeExample
{
	// native method
	public native void printMethod ();
	static
	{
		// The name of DLL file
		System.loadLibrary ("LibraryName");
	}
	public static void main (String[] args)
	{
		NativeExample obj = new NativeExample ();
		obj.printMethod ();
	}
}
```
Output
```bash
Manoj
```
